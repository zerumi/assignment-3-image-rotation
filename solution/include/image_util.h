#ifndef IMAGE_TRANSFORMER_IMAGE_UTIL_H
#define IMAGE_TRANSFORMER_IMAGE_UTIL_H

#include "image.h"

#include <memory.h>

#define PIXELS_COUNT 9

enum blur_border_identity_padding {
    NO_PADDING = 0,
    BLACK,
    WHITE,
    REFLECTION,
    IMAGE_LOOP,
    BORDER_PIXEL
};

enum image_operation_status {
    IMAGE_OK = 0,
    IMAGE_OUT_OF_MEMORY
};

enum image_operation_status rotate(struct image src, struct image* out);
enum image_operation_status blur(struct image src, enum blur_border_identity_padding mode, struct image* out);

#endif //IMAGE_TRANSFORMER_IMAGE_UTIL_H
